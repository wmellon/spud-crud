(function() {
  'use strict';
  // Define the 'view-user' module
  // Animate is used as a render engine within Angular once a change occurs.
  // Not really being taken advantage of yet by me.
  angular.module('viewUser', ['ngRoute', 'ngAnimate']);
})();