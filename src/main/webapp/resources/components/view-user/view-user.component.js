(function() {
  'use strict';

  // Register 'viewChat' component, along with its associated controller and template
  angular.
  module('viewUser').
  component('viewUser', {
    templateUrl: 'components/view-user/view-user.template.html',
    controller: ['userService', UserController],
    controllerAs: 'user',
  });

  //Controller that is binding and retrieving info assoc with view-user template.
  function UserController(userService) {
    var self = this;

    self.firstName = "";
    self.lastName = "";
    self.date = '';
    //Kick off Service

    self.updateFlag = false; // toggles submit and update modes
    self.cancelButton = true; // hide by default
    self.updateButtonName = "Submit";

    self.users = {};
    self.tempID = -1;


    // calls select user function
    self.selectUser = function(userID) {
      self.tempID = userID; //copy the id for later use
      self.updateFlag = !self.updateFlag;
      //console.log("DEBUG - userID: " + userID);
      for(var i = 0; i < self.users.length; i++){
        if(self.users[i]._id == userID) {
          self.firstName = self.users[i].firstName;
          self.lastName = self.users[i].lastName;
          self.date = self.users[i].date;
          break;
        }
      }
      self.cancelButton = false;
      self.updateButtonName = "Update";
    }

    // cancel update mode
    self.cancel = function() {
      self.tempID = -1;
      self.firstName = "";
      self.lastName = "";
      self.date = '';
      self.cancelButton = true;
      self.updateButtonName = "Submit";
      self.updateFlag = !self.updateFlag;
    }
    
    // calls getUsers for filling table content
    userService.getUsers(function(success) {
      console.log(success);
      self.users = success.data;
    });

    // calls deleteUser
    self.delete = function(userID) {
      console.log("userID: " + userID);
      userService.deleteUser(userID, function(success) {
        console.log(success);
        for(var i = 0; i < self.users.length; i++) {
          if(self.users[i]._id == userID) {
            self.users.splice(i, 1);
            break;
          }
        }
      });
    }

    self.submit = function() {
      //createUser defined in service.
      //Pass info.
      var tmpUser = {
        firstName: self.firstName,
        lastName: self.lastName,
        date: self.date,
      };

      if(!self.updateFlag) {
        userService.createUser(tmpUser, function(success) {
        		//console.log("Successmessage: "+JSON.stringify(success.data._id));
        		
        		//adds id and rev to tmp user to allow proper deletion on client side without refresh
        		tmpUser._id = success.data._id;
        		tmpUser._rev = success.data._rev;
        		self.users.push(tmpUser);
        		
        });
        
      }
      else {
        // update user
        userService.updateUser(tmpUser, self.tempID, function (success) {
          // update view
          for(var i = 0; i < self.users.length; i++) {
            if(self.users[i]._id == self.tempID) {
              self.users[i].firstName = self.firstName;
              self.users[i].lastName = self.lastName;
              self.users[i].date = self.date;
              self.tempID = -1;
              break;
            }
          }
        });
      }
    }
  }
})();