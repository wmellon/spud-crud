package com.ibm.cic;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cloudant.client.api.model.Response;

@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	CloudantService service;

	public UserController() {}

	@RequestMapping(path = "", method = RequestMethod.POST)
	public String createUser(@RequestBody(required = false) User u) {
		if (u == null) {
			u = new User();
			
		}
		this.service.post(u);
		
		return u.toString();
	}

	@RequestMapping(path = "", method = RequestMethod.GET)
	public List<User> viewUsers() {
		return this.service.getAllUsers();
	}

	@RequestMapping(path = "{id}", method = RequestMethod.PUT)
	public boolean updateUser(@PathVariable("id") String id, @RequestBody(required = false) User u) {
		User user = service.findDocById(id);
		user.setFirstName(u.getFirstName());
		user.setLastName(u.getLastName());
		user.setDate(u.getDate());
		this.service.put(user);
		
		return true;
	}

	@RequestMapping(path = "{id}", method = RequestMethod.DELETE)
	public boolean deleteUser(@PathVariable("id") String id) {
		
		System.out.println("deleteUser() id = " + id);
		return this.service.delete(this.service.findDocById(id));
	}

}

