package com.ibm.cic;

/**
 * This class creates a User object with no arguments, with getter
 * and setter methods for each field.
 */
class User {
	
	private String _id;
	private String _rev;
	private String firstName;
	private String lastName;
	private long date;
	
	/**
	 * Returns the _id of the document.
	 * 
	 * @return - String
	 */
	public String get_id() {
		return _id;
	}

	/**
	 * Sets the _id of the document.
	 * 
	 * @param _id - String
	 */
	public void set_id(String _id) {
		this._id = _id;
	}

	/**
	 * Returns the _rev of the document.
	 * 
	 * @return - String
	 */
	public String get_rev() {
		return _rev;
	}

	/**
	 * Sets the _rev of the document.
	 * 
	 * @param _id - String
	 */
	public void set_rev(String _rev) {
		this._rev = _rev;
	}

	/**
	 * Returns the firstName of the document.
	 * 
	 * @return - String
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the firstName of the document.
	 * 
	 * @param _id - String
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Returns the lastName of the document.
	 * 
	 * @return - String
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the lastName of the document.
	 * 
	 * @param _id - String
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Returns the date of the document.
	 * 
	 * @return - String
	 */
	public long getDate() {
		return date;
	}

	/**
	 * Sets the date of the document.
	 * 
	 * @param _id - long
	 */
	public void setDate(long date) {
		this.date = date;
	}

	/**
	 * Returns a String in JSON form.
	 */
	public String toString() {
		return "{\n\t\"_id\": \"" + _id + "\",\n"
				+ "\t\"_rev\": \"" + _rev + "\",\n"
				+ "\t\"firstName\": \"" + firstName + "\",\n"
				+ "\t\"lastName\": \"" + lastName + "\",\n"
				+ "\t\"date\": \"" + date + "\"\n"
				+ "}";
	}
}